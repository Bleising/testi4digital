"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan")); //Nos permite ver por consola las peticiones que van llegando
//helmet: módulo para seguridad de API
const helmet_1 = __importDefault(require("helmet")); //Nos ayuda con seguridad de algunas validaciones atomatizadas de seguridad de API
const indexRoute_1 = __importDefault(require("./routes/indexRoute")); //Importamos el módulo de enrutamiento
const mongoose_1 = __importDefault(require("mongoose"));
const compression_1 = __importDefault(require("compression"));
const cors_1 = __importDefault(require("cors"));
const requestsController_1 = __importDefault(require("./controllers/requestsController"));
const usersController_1 = __importDefault(require("./controllers/usersController"));
class Server {
    constructor() {
        //Ejecutamos express
        this.app = express_1.default();
        //Asignamos la configuración
        this.config();
        //Cargamos las rutas
        this.routes();
    }
    //Establece la configuración del servidor
    config() {
        const MONGO_URI = 'mongodb+srv://adm:adm123321@cluster1.c0bxl.mongodb.net/restapidb';
        mongoose_1.default.set('useFindAndModify', false);
        mongoose_1.default.connect(process.env.MONGODB_URL || MONGO_URI, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true
        }).then(db => {
            console.log('DB is connected');
        });
        //settings
        this.app.set('port', process.env.PORT || 3000);
        //Middlewares
        this.app.use(morgan_1.default('dev'));
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: false })); //Para soportar también los envíos desde formularios
        this.app.use(helmet_1.default());
        this.app.use(compression_1.default());
        this.app.use(cors_1.default());
    }
    //Determina las rutas
    routes() {
        //Le decimos que utilice el archivo de indexRoutes para enrutar
        this.app.use(indexRoute_1.default);
        this.app.use('/api', requestsController_1.default);
        this.app.use('/api', usersController_1.default);
        // this.app.get('/',(req, res) => res.send('Hello World'));
    }
    //Inicializa el servidor
    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log('Server listening on port', this.app.get('port'));
        });
    }
}
const server = new Server();
server.start();
