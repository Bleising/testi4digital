"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClientAPI = void 0;
const node_fetch_1 = __importDefault(require("node-fetch"));
class ClientAPI {
    constructor() {
    }
    getUsers() {
        return __awaiter(this, void 0, void 0, function* () {
            let result;
            yield node_fetch_1.default('https://jsonplaceholder.typicode.com/users')
                .then(response => response.json())
                .then(json => {
                //console.log(json)
                result = json;
            });
            return result;
        });
    }
    getPublications() {
        return __awaiter(this, void 0, void 0, function* () {
            let result;
            yield node_fetch_1.default('https://jsonplaceholder.typicode.com/posts')
                .then(response => response.json())
                .then(json => {
                //console.log(json)
                result = json;
            });
            return result;
        });
    }
    getPhotos() {
        return __awaiter(this, void 0, void 0, function* () {
            let result;
            yield node_fetch_1.default('https://jsonplaceholder.typicode.com/photos')
                .then(response => response.json())
                .then(json => {
                //console.log(json)
                result = json;
            });
            return result;
        });
    }
    getAlbums() {
        return __awaiter(this, void 0, void 0, function* () {
            let result;
            yield node_fetch_1.default('https://jsonplaceholder.typicode.com/albums')
                .then(response => response.json())
                .then(json => {
                //console.log(json)
                result = json;
            });
            return result;
        });
    }
}
exports.ClientAPI = ClientAPI;
