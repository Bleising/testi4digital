"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const RequestSchema = new mongoose_1.Schema({
    requestId: { type: String, required: true, unique: true },
    date: { type: Date, default: Date.now },
    method: { type: String, required: true, lowercase: true },
    data: { type: String, required: false }
});
exports.default = mongoose_1.model('Request', RequestSchema); //
