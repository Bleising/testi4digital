"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestRepository = void 0;
const request_1 = __importDefault(require("../models/request"));
const uuid_1 = require("uuid");
class RequestRepository {
    constructor() {
    }
    getRequests() {
        return __awaiter(this, void 0, void 0, function* () {
            const requests = yield request_1.default.find();
            return requests;
        });
    }
    deleteRequest(requestId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield request_1.default.deleteOne({ requestId: `${requestId}` });
            return { data: "Elemento eliminado  correctamente" };
        });
    }
    updateRequest(requestId, data) {
        return __awaiter(this, void 0, void 0, function* () {
            yield request_1.default.updateOne({ requestId: `${requestId}` }, data);
            return data;
        });
    }
    createRequest(data) {
        return __awaiter(this, void 0, void 0, function* () {
            let myuuid = uuid_1.v4();
            let requestId = `${myuuid}`;
            const newRequest = new request_1.default({ requestId: requestId, date: data.date, method: data.method, data: data.data });
            //console.log(newRequest);
            yield newRequest.save();
            return newRequest;
        });
    }
}
exports.RequestRepository = RequestRepository;
