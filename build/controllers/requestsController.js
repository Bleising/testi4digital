"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const requestDomain_1 = require("../domain/request/requestDomain");
const requestEntity_1 = require("../domain/request/requestEntity");
const requestRepository_1 = require("../infrastructure/repositories/requestRepository");
class RequestsController {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    getRequests(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let repository = new requestRepository_1.RequestRepository();
            let domain = new requestDomain_1.RequestDomain(repository);
            const requests = yield domain.getRequests();
            res.json(requests);
        });
    }
    deleteRequest(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const requestId = req.params.requestId;
            let repository = new requestRepository_1.RequestRepository();
            let domain = new requestDomain_1.RequestDomain(repository);
            const requests = yield domain.deleteRequest(requestId);
            res.json(requests);
        });
    }
    updateRequest(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const requestId = req.params.requestId;
            const { date, method, data } = req.body;
            console.log(requestId);
            const newRequest = new requestEntity_1.RequestEntity();
            newRequest.date = date;
            newRequest.method = method; //
            newRequest.data = data;
            let repository = new requestRepository_1.RequestRepository();
            let domain = new requestDomain_1.RequestDomain(repository);
            const requests = yield domain.updateRequest(requestId, newRequest);
            res.json(requests);
        });
    }
    // async createRequest(req : Request, res: Response){
    //     const {date, method, data } = req.body;
    //     const newRequest = new RequestEntity();
    //     newRequest.date = date;
    //     newRequest.method = method;//
    //     newRequest.data = data;
    //     let repository = new  RequestRepository();
    //     let domain = new RequestDomain(repository);
    //     const requests = await domain.createRequest(newRequest);
    //     res.json(requests);
    // }
    routes() {
        this.router.get('/requests', this.getRequests);
        this.router.put('/requests/:requestId', this.updateRequest);
        this.router.delete('/requests/:requestId', this.deleteRequest);
        // this.router.post('/requests', this.createRequest);
    }
}
const requestsController = new RequestsController();
exports.default = requestsController.router;
