"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const requestDomain_1 = require("../domain/request/requestDomain");
const requestEntity_1 = require("../domain/request/requestEntity");
const userDomain_1 = require("../domain/user/userDomain");
const requestRepository_1 = require("../infrastructure/repositories/requestRepository");
const userRepository_1 = require("../infrastructure/repositories/userRepository");
class UsersController {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    getUsers(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let repository = new userRepository_1.UserRepository();
            let domain = new userDomain_1.UserDomain(repository);
            const requests = yield domain.getUsers();
            //Guardamos la traza de la solicitud
            const newRequest = new requestEntity_1.RequestEntity();
            newRequest.date = new Date();
            newRequest.method = "getUsers";
            newRequest.data = JSON.stringify(requests);
            let requestRepository = new requestRepository_1.RequestRepository();
            let requestDomain = new requestDomain_1.RequestDomain(requestRepository);
            yield requestDomain.createRequest(newRequest);
            //==================================
            res.json(requests);
        });
    }
    getPublications(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let repository = new userRepository_1.UserRepository();
            let domain = new userDomain_1.UserDomain(repository);
            const requests = yield domain.getPublications();
            //Guardamos la traza de la solicitud
            const newRequest = new requestEntity_1.RequestEntity();
            newRequest.date = new Date();
            newRequest.method = "getUsers";
            newRequest.data = JSON.stringify(requests);
            let requestRepository = new requestRepository_1.RequestRepository();
            let requestDomain = new requestDomain_1.RequestDomain(requestRepository);
            yield requestDomain.createRequest(newRequest);
            //==================================
            res.json(requests);
        });
    }
    getPhotosByUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const usertId = req.params.userId;
            console.log(usertId);
            let repository = new userRepository_1.UserRepository();
            let domain = new userDomain_1.UserDomain(repository);
            const requests = yield domain.getPHotosByUserId(Number(usertId));
            //Guardamos la traza de la solicitud
            const newRequest = new requestEntity_1.RequestEntity();
            newRequest.date = new Date();
            newRequest.method = "getUsers";
            newRequest.data = JSON.stringify(requests);
            let requestRepository = new requestRepository_1.RequestRepository();
            let requestDomain = new requestDomain_1.RequestDomain(requestRepository);
            yield requestDomain.createRequest(newRequest);
            //==================================
            res.json(requests);
        });
    }
    routes() {
        this.router.get('/users', this.getUsers);
        this.router.get('/posts', this.getPublications);
        this.router.get('/photos/:userId', this.getPhotosByUser);
        // this.router.put('/requests/:requestId', this.updateRequest);
        // this.router.delete('/requests/:requestId', this.deleteRequest);
        // this.router.post('/requests', this.createRequest);
    }
}
const userController = new UsersController();
exports.default = userController.router;
