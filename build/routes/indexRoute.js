"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
class IndexRoute {
    constructor() {
        //Se encarga de ejecutar el enrutador
        this.router = express_1.Router();
        this.routes();
    }
    //Determina las rutas
    routes() {
        //this.router.get('/', (req, res) => res.send('https://app.swaggerhub.com/apis-docs/juanOrtega/API4digital/1.0.0'));
        this.router.get('/', (req, res) => res.redirect('https://app.swaggerhub.com/apis-docs/juanOrtega/API4digital/1.0.0'));
    }
}
const indexRoute = new IndexRoute();
indexRoute.routes();
exports.default = indexRoute.router;
