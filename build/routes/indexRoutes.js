"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
class IndexRoutes {
    constructor() {
        this.router = express_1.Router();
    }
    //Determina las rutas
    routes() {
        this.router.get('/', (req, res) => res.send('Hello World'));
    }
}
