"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestDomain = void 0;
class RequestDomain {
    constructor(requestRepository) {
        this.repository = requestRepository;
    }
    getRequests() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.repository.getRequests();
            }
            catch (error) {
            }
        });
    }
    createRequest(data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.repository.createRequest(data);
            }
            catch (error) {
            }
        });
    }
    updateRequest(requestId, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.repository.updateRequest(requestId, data);
            }
            catch (error) {
            }
        });
    }
    deleteRequest(requestId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.repository.deleteRequest(requestId);
            }
            catch (error) {
            }
        });
    }
}
exports.RequestDomain = RequestDomain;
