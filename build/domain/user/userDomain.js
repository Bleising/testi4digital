"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserDomain = void 0;
class UserDomain {
    constructor(userRepository) {
        this.repository = userRepository;
    }
    getUsers() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.repository.getUsers();
            }
            catch (error) {
            }
        });
    }
    getPublications() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.repository.getPublications();
            }
            catch (error) {
            }
        });
    }
    getPHotosByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let albums = yield this.repository.getAlbums();
                let photos = yield this.repository.getPhotos();
                let albumsByUser = albums.filter((x) => x.userId === userId);
                let lstPhotos = new Array();
                for (let index = 0; index < albumsByUser.length; index++) {
                    const element = albumsByUser[index];
                    let lstPhotosTemp = photos.filter((x) => Number(x.albumId) === Number(element.id));
                    lstPhotosTemp.forEach((element) => {
                        lstPhotos.push(element);
                    });
                }
                return lstPhotos;
            }
            catch (error) {
            }
        });
    }
}
exports.UserDomain = UserDomain;
