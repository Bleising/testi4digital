# Testi4digital

Este proyecto es creado para la prueba técnica de la empresa i4digital.

	Comandos a ejecutar para compilar el proyecto:
	
	Primero instalar las dependencias: *npm i*
	Si desea hacer cambios puede ejecutar *npm run ts* para reiniciar el servidor automáticamente cuando existan cambios.
	Para ejecutar el proyecto en modo de desarrollo usar *npm run dev*
	Para transpilar y ejecutar el proyecto a producción usar el comando *npm run start*
	Para ejecutar las pruebas unitarias usar el comando *npm run test* - Antes de esto asegúrese de que su servidor se esté ejecutando correctamente en el puerto 3000 y que tenga instalado todas las dependencias.
    Si al ejecutar el comando el servidor se está ejecutando en el puerto 3000 y lanza algún error referente a la ruta de ejecución del comando (./node_modules/.bin/mocha src/test) reemplacela con la siguiente e inténtelo de nuevo: node_modules/.bin/mocha src/test
	
	El proyecto está configurado para ejecutarse en el puerto 3000.
	
	Las credenciales de acceso a la base de datos son las siguientes:
	
	mongodb+srv://adm:adm123321@cluster1.c0bxl.mongodb.net/restapidb
	
	Arquitectura:
	
	La arquitectura del proyecto está basada en una arquitectura guiada por dominio simplificada (sin interfaes entre el dominio y el controlador)
	esto para hacer mejor uso de los repositorios mock de pruebas y para poder aplicar principios SOLID sobre la solución.
	
	Documentación:
	
	Pueden encontrar mas información de los servicios en su documentación swagger expuesta en la siguiente url:
	https://app.swaggerhub.com/apis-docs/juanOrtega/API4digital/1.0.0

    Pruebas de API:

    NAME: Obtener Solicitudes
    GET: http://localhost:3000/api/requests

    NAME: Actualizar Solicitud
    PUT: http://localhost:3000/api/requests/1
    BODY:
    {
    "date":"12/08/2022",
    "method":"test Method updated4",
    "data":"Test Update"
    }

    NAME: Eliminar Solicitudes
    DELETE: http://localhost:3000/api/requests/1

    NAME: Obtener Usuarios
    GET: http://localhost:3000/api/users

    NAME: Obtener Publicaciones
    GET: http://localhost:3000/api/posts

    NAME: Obtener Fotos De Un Usuario
    GET: http://localhost:3000/api/photos/1 
