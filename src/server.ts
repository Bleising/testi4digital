import express from 'express';
import morgan from 'morgan';//Nos permite ver por consola las peticiones que van llegando
//helmet: módulo para seguridad de API
import helmet from 'helmet';//Nos ayuda con seguridad de algunas validaciones atomatizadas de seguridad de API
import indexRoute from './routes/indexRoute';//Importamos el módulo de enrutamiento
import mongoose from 'mongoose';
import compression from 'compression';
import cors from 'cors';
import RequestsController from './controllers/requestsController';
import UsersController from './controllers/usersController';



class Server {
    app: express.Application;
    constructor() {
        //Ejecutamos express
        this.app = express();
        //Asignamos la configuración
        this.config();
        //Cargamos las rutas
        this.routes();
    }

    //Establece la configuración del servidor
    config() {
        const MONGO_URI = 'mongodb+srv://adm:adm123321@cluster1.c0bxl.mongodb.net/restapidb';
        mongoose.set('useFindAndModify', false);
        mongoose.connect(process.env.MONGODB_URL || MONGO_URI, {
            useNewUrlParser: true,//Relacionado a mongoose
            useCreateIndex: true,
            useUnifiedTopology: true
        }).then(db => {
            console.log('DB is connected');
        });
        //settings
        this.app.set('port', process.env.PORT || 3000);
        //Middlewares
        this.app.use(morgan('dev'));
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));//Para soportar también los envíos desde formularios
        this.app.use(helmet());
        this.app.use(compression());
        this.app.use(cors());

    }

    //Determina las rutas
    routes() {
        //Le decimos que utilice el archivo de indexRoutes para enrutar
        this.app.use(indexRoute);
        this.app.use('/api',RequestsController);
        this.app.use('/api',UsersController);

        // this.app.get('/',(req, res) => res.send('Hello World'));
    }

    //Inicializa el servidor
    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log('Server listening on port', this.app.get('port'));
        });
    }
}

const server = new Server();
server.start();