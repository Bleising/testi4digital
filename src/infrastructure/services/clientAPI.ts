import fetch from 'node-fetch';
export class ClientAPI {
    constructor() {

    }
    async getUsers() {
        let result: any;
        await fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(json => {
                //console.log(json)
                result = json;
            })
        return result;
    }

    async getPublications() {
        let result: any;
        await fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(json => {
                //console.log(json)
                result = json;
            })
        return result;
    }

    async getPhotos() {
        let result: any;
        await fetch('https://jsonplaceholder.typicode.com/photos')
            .then(response => response.json())
            .then(json => {
                //console.log(json)
                result = json;
            })
        return result;
    }


    async getAlbums() {
        let result: any;
        await fetch('https://jsonplaceholder.typicode.com/albums')
            .then(response => response.json())
            .then(json => {
                //console.log(json)
                result = json;
            })
        return result;
    }

}
