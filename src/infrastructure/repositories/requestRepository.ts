import { RequestEntity } from '../../domain/request/requestEntity';
import Request from '../models/request';
import { v4 as uuidv4 } from 'uuid';

export class RequestRepository implements IRequest {

    constructor() {

    }

    async getRequests() {
        const requests = await Request.find();
        return requests;
    }

    async deleteRequest(requestId: any) {
        await Request.deleteOne({ requestId: `${requestId}` });
        return { data: "Elemento eliminado  correctamente" };
    }
    async updateRequest(requestId: any, data: RequestEntity) {
        await Request.updateOne({ requestId: `${requestId}` }, data);
        return data;
    }
    async createRequest(data: RequestEntity) {
        let myuuid = uuidv4();
        let requestId: string = `${myuuid}`;
        const newRequest = new Request({ requestId: requestId, date: data.date, method: data.method, data: data.data });
        //console.log(newRequest);
        await newRequest.save();
        return newRequest;
    }


}
