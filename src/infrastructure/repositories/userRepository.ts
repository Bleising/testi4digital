import { ClientAPI } from "../services/clientAPI"

export class UserRepository implements IUser
{
    constructor(){

    }

    async getUsers() {
        const api = new ClientAPI();
        const users = await api.getUsers();
        console.log("gggggg", users);
        return users;
    }

    async getPublications() {
        const api = new ClientAPI();
        const users = await api.getPublications();
        return users;
    }

    async getAlbums() {
        const api = new ClientAPI();
        const users = await api.getAlbums();
        return users;
    }

    async getPhotos() {
        const api = new ClientAPI();
        const users = await api.getPhotos();
        return users;
    }

}