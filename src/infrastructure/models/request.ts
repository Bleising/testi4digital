import { Schema, model } from 'mongoose';

const RequestSchema = new Schema({
    requestId: { type: String, required: true, unique: true },
    date: { type: Date, default: Date.now },
    method:{ type: String, required: true, lowercase: true },
    data:{ type: String, required: false }
});

export default model('Request', RequestSchema);//