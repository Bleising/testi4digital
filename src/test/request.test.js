const assert = require('assert');
var supertest = require("supertest");
var should = require("should");

var server = supertest.agent("http://localhost:3000");

describe('Simple Math Test', () => {

    it('test users controller', () => {
        server
        .get("/api/users")
        .expect("Content-type", /json/)
        .expect(200) // THis is HTTP response
        .end(function (err, res) {
            res.statusCode.should.equal(200);
        });
    });

    it('test posts controller', () => {
        server
        .get("/api/posts")
        .expect("Content-type", /json/)
        .expect(200) // THis is HTTP response
        .end(function (err, res) {
            res.statusCode.should.equal(200);
        });
    });

    it('Test photos method', () => {
        server
            .get("/api/photos/1")
            .expect("Content-type", /json/)
            .expect(200) // THis is HTTP response
            .end(function (err, res) {
                //console.log(res);
                //res.statusCode.equal(200);
                // HTTP status should be 200
                res.statusCode.should.equal(200);
                // Error key should be false.
                //res.body.error.should.equal(false);
                //done();
            });
    });
});

