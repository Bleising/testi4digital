import { Request, Response, Router } from 'express';
import { RequestDomain } from '../domain/request/requestDomain';
import { RequestEntity } from '../domain/request/requestEntity';
import { UserDomain } from '../domain/user/userDomain';
import { RequestRepository } from '../infrastructure/repositories/requestRepository';
import { UserRepository } from '../infrastructure/repositories/userRepository';

class UsersController {
    router: Router;
    constructor() {
        this.router = Router();
        this.routes();
    }

    async getUsers(req: Request, res: Response) {
        let repository = new UserRepository();
        let domain = new UserDomain(repository);
        const requests = await domain.getUsers();
        //Guardamos la traza de la solicitud
        const newRequest = new RequestEntity();
        newRequest.date = new Date();
        newRequest.method = "getUsers";
        newRequest.data = JSON.stringify(requests);
        let requestRepository = new RequestRepository();
        let requestDomain = new RequestDomain(requestRepository);
        await requestDomain.createRequest(newRequest);
        //==================================
        res.json(requests);
    }

    async getPublications(req: Request, res: Response) {
        let repository = new UserRepository();
        let domain = new UserDomain(repository);
        const requests = await domain.getPublications();
        //Guardamos la traza de la solicitud
        const newRequest = new RequestEntity();
        newRequest.date = new Date();
        newRequest.method = "getUsers";
        newRequest.data = JSON.stringify(requests);
        let requestRepository = new RequestRepository();
        let requestDomain = new RequestDomain(requestRepository);
        await requestDomain.createRequest(newRequest);
        //==================================
        res.json(requests);
    }

    async getPhotosByUser(req: Request, res: Response) {
        const usertId = req.params.userId;
        console.log(usertId);
        let repository = new UserRepository();
        let domain = new UserDomain(repository);
        const requests = await domain.getPHotosByUserId(Number(usertId));
        //Guardamos la traza de la solicitud
        const newRequest = new RequestEntity();
        newRequest.date = new Date();
        newRequest.method = "getUsers";
        newRequest.data = JSON.stringify(requests);
        let requestRepository = new RequestRepository();
        let requestDomain = new RequestDomain(requestRepository);
        await requestDomain.createRequest(newRequest);
        //==================================
        res.json(requests);
    }

    routes() {
        this.router.get('/users', this.getUsers);
        this.router.get('/posts', this.getPublications);
        this.router.get('/photos/:userId', this.getPhotosByUser);
        // this.router.put('/requests/:requestId', this.updateRequest);
        // this.router.delete('/requests/:requestId', this.deleteRequest);
        // this.router.post('/requests', this.createRequest);
    }
}
const userController = new UsersController();
export default userController.router;