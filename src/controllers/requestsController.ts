import { Request, Response, Router } from 'express';
import { RequestDomain } from '../domain/request/requestDomain';
import { RequestEntity } from '../domain/request/requestEntity';
import { RequestRepository } from '../infrastructure/repositories/requestRepository';

class RequestsController {
    router: Router;
    constructor() {
        this.router = Router();
        this.routes();
    }

    async getRequests(req : Request, res: Response){
        let repository = new  RequestRepository();
        let domain = new RequestDomain(repository);
        const requests = await domain.getRequests();
        res.json(requests);
    }

    async deleteRequest(req : Request, res: Response){
        const requestId = req.params.requestId;
        let repository = new  RequestRepository();
        let domain = new RequestDomain(repository);
        const requests = await domain.deleteRequest(requestId);
        res.json(requests);
    }

    async updateRequest(req : Request, res: Response)
    {
        const requestId = req.params.requestId;
        const {date, method, data } = req.body;
        console.log(requestId);
        const newRequest = new RequestEntity();
        newRequest.date = date;
        newRequest.method = method;//
        newRequest.data = data;
        let repository = new  RequestRepository();
        let domain = new RequestDomain(repository);
        const requests = await domain.updateRequest(requestId, newRequest);
        res.json(requests);
    }

    // async createRequest(req : Request, res: Response){
    //     const {date, method, data } = req.body;
    //     const newRequest = new RequestEntity();
    //     newRequest.date = date;
    //     newRequest.method = method;//
    //     newRequest.data = data;
    //     let repository = new  RequestRepository();
    //     let domain = new RequestDomain(repository);
    //     const requests = await domain.createRequest(newRequest);
    //     res.json(requests);
    // }
    routes(){
        this.router.get('/requests', this.getRequests);
        this.router.put('/requests/:requestId', this.updateRequest);
        this.router.delete('/requests/:requestId', this.deleteRequest);
        // this.router.post('/requests', this.createRequest);
    }
}

const requestsController = new RequestsController();
export default requestsController.router;