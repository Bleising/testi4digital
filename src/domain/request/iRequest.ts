interface IRequest {
    getRequests(): any;
    deleteRequest(requestId: any): any;
    updateRequest(requestId:any, data: any): any;
    createRequest(data: any): any;
}