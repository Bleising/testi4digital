import { RequestEntity } from "./requestEntity";

export class RequestDomain {
    private readonly repository: IRequest;

    constructor(requestRepository: IRequest) {
        this.repository = requestRepository;
    }

    async getRequests() {
        try {
            return await this.repository.getRequests();
        } catch (error) {

        }
    }

    async createRequest(data: RequestEntity) {
        try {
            return await this.repository.createRequest(data);
        } catch (error) {

        }
    }

    async updateRequest(requestId:any, data: RequestEntity) {
        try {
            return await this.repository.updateRequest(requestId,data);
        } catch (error) {

        }
    }

    async deleteRequest(requestId: any) {
        try {
            return await this.repository.deleteRequest(requestId);
        } catch (error) {

        }
    }
}