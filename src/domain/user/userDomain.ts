
export class UserDomain {
    private readonly repository: IUser;
    constructor(userRepository: IUser) {
        this.repository = userRepository;
    }

    async getUsers() {
        try {
            return await this.repository.getUsers();
        } catch (error) {

        }
    }

    async getPublications() {
        try {
            return await this.repository.getPublications();
        } catch (error) {

        }
    }

    async getPHotosByUserId(userId: number) {
        try {
            let albums = await this.repository.getAlbums();

            let photos = await this.repository.getPhotos();
            let albumsByUser = albums.filter((x: any) => x.userId === userId);
            let lstPhotos = new Array<any>();
            for (let index = 0; index < albumsByUser.length; index++) {
                const element = albumsByUser[index];
                let lstPhotosTemp = photos.filter((x: any) => Number(x.albumId) === Number(element.id));
                lstPhotosTemp.forEach((element: any) => {
                    lstPhotos.push(element);
                });
            }
            return lstPhotos;
        } catch (error) {

        }
    }


}