import { Request, Response, Router, Express } from 'express';

class IndexRoute {

    router: Router;
    constructor() {
        //Se encarga de ejecutar el enrutador
        this.router = Router();
        this.routes();
    }
    //Determina las rutas
    routes() {
        //this.router.get('/', (req, res) => res.send('https://app.swaggerhub.com/apis-docs/juanOrtega/API4digital/1.0.0'));
        this.router.get('/', (req, res) => res.redirect('https://app.swaggerhub.com/apis-docs/juanOrtega/API4digital/1.0.0'));
    }
}

const indexRoute = new IndexRoute();
indexRoute.routes();

export default indexRoute.router;